package org.jf.dexlib2.dexbacked;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.iface.instruction.Instruction;
import org.jf.dexlib2.iface.instruction.TwoRegisterInstruction;
/**
 * This class detects very simple opaque predicates.
 */
public class DexBackedInstructionAnalyzer {
			
	public static boolean canContinue(Instruction instruction) {
		
		final Opcode opcode = instruction.getOpcode();
		
		switch (opcode.format)
		{
		case Format22t:
			//Handles the simple anti-disass trick if-eq vx, vx, +m
			if (opcode == Opcode.IF_EQ || opcode == Opcode.IF_GE  || opcode == Opcode.IF_LE)
			{
			
				if (((TwoRegisterInstruction) instruction).getRegisterA() == ((TwoRegisterInstruction) instruction).getRegisterB())
				{
					return false;
				}
			}
			
		default: return opcode.canContinue();
			
		}

	}
	//Returns True if the instruction can jump to its target location
	public static boolean canJump(Instruction instruction) {

		final Opcode opcode = instruction.getOpcode();
		
		switch (opcode.format)
		{
		case Format22t:
			
			if (opcode == Opcode.IF_NE)
			{
				//A register is always equal to itself so "if not equals" will always evaluate to false, so it will never jump
				if (((TwoRegisterInstruction) instruction).getRegisterA() == ((TwoRegisterInstruction) instruction).getRegisterB())
				{
					return false;
				}
			}
		
		case Format10t:
		case Format20t:
		case Format21t:
		case Format30t:
		case Format31t:
			return true;
			
		default: return false;
			
		}

	}
}
