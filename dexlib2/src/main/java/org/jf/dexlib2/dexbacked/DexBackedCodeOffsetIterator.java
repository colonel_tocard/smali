package org.jf.dexlib2.dexbacked;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

import javax.annotation.Nonnull;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.dexbacked.instruction.DexBackedInstruction;
import org.jf.dexlib2.iface.instruction.Instruction;
import org.jf.dexlib2.iface.instruction.OffsetInstruction;
import org.jf.dexlib2.iface.instruction.SwitchElement;
import org.jf.dexlib2.iface.instruction.SwitchPayload;
import org.jf.util.ExceptionWithContext;

/**
 * Given an instruction, this iterator will yield the possible code locations executable after this instruction.
 */
public class DexBackedCodeOffsetIterator implements Iterator<Integer> {
	
	private final Stack<Integer> st;
	private final DexReader dexreader;
	private Integer nextitem = null;
	
	public DexBackedCodeOffsetIterator(Instruction instruction, int codeoffset, int codeend, DexReader dexreader) {

		st = new Stack<Integer>();
		this.dexreader = dexreader;
		
		//Check if we spot a known anti-disassembly trick, like eq v0, v0 + xx
		if (DexBackedInstructionAnalyzer.canContinue(instruction) && ((instruction.getCodeUnits()*2 + codeoffset) < codeend))
		{	
			st.push(instruction.getCodeUnits()*2 + codeoffset);	
		}
		
		//Check if we spot a known anti-disassembly trick, like if_ne v0, v0 +xx
		if (DexBackedInstructionAnalyzer.canJump(instruction) && ((((OffsetInstruction) instruction).getCodeOffset()*2 + codeoffset) < codeend))
		{
			st.push(((OffsetInstruction) instruction).getCodeOffset()*2 + codeoffset);
		}		
			
		//Add Switch Targets as well
		
		if (instruction.getOpcode() == Opcode.PACKED_SWITCH)
		{
			SwitchPayload payload = findSwitchPayload(((OffsetInstruction) instruction).getCodeOffset()*2 + codeoffset, Opcode.PACKED_SWITCH_PAYLOAD);
			for (Iterator<? extends SwitchElement> switchTargetIter = payload.getSwitchElements().iterator(); switchTargetIter.hasNext();)
			{
				st.push(switchTargetIter.next().getOffset()*2+ codeoffset);
			}
		}
		if (instruction.getOpcode() == Opcode.SPARSE_SWITCH)
		{
			SwitchPayload payload = findSwitchPayload(((OffsetInstruction) instruction).getCodeOffset()*2 + codeoffset, Opcode.SPARSE_SWITCH_PAYLOAD);
			for (Iterator<? extends SwitchElement> switchTargetIter = payload.getSwitchElements().iterator(); switchTargetIter.hasNext();)
			{
				st.push(switchTargetIter.next().getOffset()*2 + codeoffset);
			}
		}
			
        if (!st.empty())
	        {
	        	nextitem = st.pop();
	        }
		
	}
	
	
	public SwitchPayload findSwitchPayload(int targetOffset, Opcode type)
	
	{
		boolean found = false;
		dexreader.setOffset(targetOffset);
        int opcodeValue = dexreader.peekUbyte();

        if (opcodeValue == 0) {
            opcodeValue = dexreader.peekUshort();
        }
		
        if (opcodeValue != type.value)
        {
        	
        	if (opcodeValue == Opcode.NOP.value)
        	{
        		targetOffset += 2;
        		dexreader.setOffset(targetOffset);
                
        		opcodeValue = dexreader.peekUbyte();

                if (opcodeValue == 0) {
                    opcodeValue = dexreader.peekUshort();
                }
                
                if (opcodeValue == type.value)
                {
                	found = true;
                }
                
        	}
        	
        }
        else
        {
        	found = true;
        }
        
        
        if (found)
        {
            Opcode opcode = dexreader.dexBuf.getOpcodes().getOpcodeByValue(opcodeValue);
            return (SwitchPayload) DexBackedInstruction.buildInstruction(dexreader.dexBuf, opcode, targetOffset);
        }
        
        else
        {
        	throw new InvalidSwitchPayload(targetOffset);
        }
        
	}
	
    @Override
    @Nonnull
    public Integer next() {
        if (nextitem == null) {
            throw new NoSuchElementException();
        }
        Integer ret = nextitem;
        if (st.empty())
        {
        	nextitem = null;
        }
        else
        {
        	nextitem = st.pop();
        }
        
        return ret;
    }


	@Override public void remove() { throw new UnsupportedOperationException(); }

	@Override
	public boolean hasNext() {
		return (nextitem != null);
	}
	
    public static class InvalidSwitchPayload extends ExceptionWithContext {
        private final int payloadOffset;

        public InvalidSwitchPayload(int payloadOffset) {
            super("No switch payload at offset: %d", payloadOffset);
            this.payloadOffset = payloadOffset;
        }

        public int getPayloadOffset() {
            return payloadOffset;
        }
    }

}
