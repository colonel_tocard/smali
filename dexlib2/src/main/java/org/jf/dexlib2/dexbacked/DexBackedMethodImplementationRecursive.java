package org.jf.dexlib2.dexbacked;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Stack;

import javax.annotation.Nonnull;

import org.jf.dexlib2.Opcode;
import org.jf.dexlib2.iface.instruction.Instruction;
import org.jf.dexlib2.dexbacked.instruction.DexBackedInstruction;
import org.jf.dexlib2.dexbacked.raw.CodeItem;
import org.jf.dexlib2.dexbacked.util.VariableSizeLookaheadIterator;
/**
 * DexBackedMethodImplementationRecursive is an alternative to DexBackedMethodImplementation.
 * It follows code flow instead of linear sweep to disassemble.
 */
public class DexBackedMethodImplementationRecursive extends DexBackedMethodImplementation {
	private final int codeStart;
	private final int codeEnd;
	
	private TreeMap<Integer, Instruction> disassembledInstr;
	
	public DexBackedMethodImplementationRecursive(DexBackedDexFile dexFile, DexBackedMethod method, int codeOffset) {
		super(dexFile, method, codeOffset);
		this.codeStart = codeOffset + CodeItem.INSTRUCTION_START_OFFSET;
		this.codeEnd = this.codeStart + dexFile.readSmallUint(codeOffset + CodeItem.INSTRUCTION_COUNT_OFFSET)*2;
		disassembledInstr = new TreeMap<Integer, Instruction>();
		
	}

    @Nonnull
	@Override
	synchronized public Iterable<? extends Instruction> getInstructions() {	

		if (disassembledInstr.isEmpty())
		{
    		Instruction nexti;
    		int offset;
    		int target;
    		final Stack<Integer> disassemblyStack = new Stack<Integer>();
            //Push the entry point of the method
    		disassemblyStack.push(codeStart);
    		DexReader reader = dexFile.readerAt(codeStart);
    		DexBackedCodeOffsetIterator codeOffsetIter;
    		
    		//Push the location of the exception handlers
    		for (Iterator<? extends DexBackedTryBlock> tryBlockIter = getTryBlocks().iterator(); tryBlockIter.hasNext(); ) {
    			DexBackedTryBlock element = tryBlockIter.next();
    			for (Iterator<? extends DexBackedExceptionHandler>  exceptionHandlerIter = element.getExceptionHandlers().iterator(); exceptionHandlerIter.hasNext(); )
    			{
    				DexBackedExceptionHandler exc_handler = exceptionHandlerIter.next();
    				disassemblyStack.push(exc_handler.getHandlerCodeAddress() * 2 + codeStart);
    			}
    		}
			//Keep disassembling until our stack is empty
			while (!disassemblyStack.isEmpty())
				
			{
				offset = disassemblyStack.pop();
				reader.setOffset(offset);
        		
                int opcodeValue = reader.peekUbyte();

                if (opcodeValue == 0) {
                    opcodeValue = reader.peekUshort();
                }
                
                Opcode opcode = reader.dexBuf.getOpcodes().getOpcodeByValue(opcodeValue);
                nexti = DexBackedInstruction.buildInstruction(reader.dexBuf, opcode, offset);
                disassembledInstr.put(offset, nexti);

                codeOffsetIter = new DexBackedCodeOffsetIterator(nexti, offset, codeEnd, reader);

                //Iterate over all the possible locations for the next instruction
                while (codeOffsetIter.hasNext())
                {
                	target = codeOffsetIter.next();
                	//Check if we already disassembled this code location, if not we push it on our stack
                	if (!disassembledInstr.containsKey(target))
                	{
                	disassemblyStack.push(target);
                	}
                }
                
			}
			
			//Now let's fill the GAPs with NOP slices. #TODO : put in comment the actual raw bytes instead of NOP slices
			
			int previous_instruction_end=0;
			int current_instruction_start;
			int delta;
			int write_pt;
			Instruction instr;
			boolean isFirstIter= true;
			
			Iterator<Entry<Integer, Instruction>> entryit = disassembledInstr.entrySet().iterator();
			TreeMap<Integer, Instruction> tmpMap = new TreeMap<Integer, Instruction>();
			
			while (entryit.hasNext())
			{
				Entry<Integer, Instruction> entry = entryit.next();
				if (isFirstIter)
				{
					previous_instruction_end = entry.getKey() + entry.getValue().getCodeUnits()*2;
					isFirstIter= false;
				}
				
				else
				{
					current_instruction_start = entry.getKey();
					delta = (current_instruction_start - previous_instruction_end)/2;
					write_pt = previous_instruction_end;
					
					while (delta > 0)
					{
						instr = DexBackedInstruction.buildInstruction(dexFile, Opcode.NOP, write_pt);
						delta -= 1;
						tmpMap.put(write_pt, instr);
						write_pt += instr.getCodeUnits()*2;
					}
					
					previous_instruction_end = current_instruction_start + entry.getValue().getCodeUnits()*2;
				}
			}
			
			if (previous_instruction_end < codeEnd)
			{
				delta = codeEnd - previous_instruction_end;
				write_pt = previous_instruction_end;
				
				while (delta > 0)
				{
					instr = DexBackedInstruction.buildInstruction(dexFile, Opcode.NOP, write_pt);
					delta -= 1;
					tmpMap.put(write_pt, instr);
					write_pt += instr.getCodeUnits()*2;	
				}
				
			}
			
			disassembledInstr.putAll(tmpMap);
	}

	final Iterator<Instruction> orderedInstructionIterator = disassembledInstr.values().iterator();
			
	return new Iterable<Instruction>() {
            @Override
            public Iterator<Instruction> iterator() {
                return new VariableSizeLookaheadIterator<Instruction>(dexFile, codeStart) {
                	
                    @Override
                    protected Instruction readNextItem(@Nonnull DexReader reader) {

                    	if (orderedInstructionIterator.hasNext())
	                    	{
	                			return orderedInstructionIterator.next();
	                    	}
                    	else
	                    	{
	                    		return null;
	                    	}
                    	
                    	}
                    };
                }
            };
        }
		
		
	}

